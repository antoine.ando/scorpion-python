from math import pow, sqrt, sin, degrees, radians
import random
import csv
import matplotlib.pyplot as plt
from numpy import average, percentile

c = csv.writer(open("test.csv", "w"), delimiter=',', quoting=csv.QUOTE_NONE)

numberOfGenerations = 300
targetDistance = 600
populationSize = 500
mutationChances = 0.5
averageDistancePoints = []
firstQuartilePoints = []
thirdQuartilePoints = []
kineticPoints = []

bf = 0.5
hf = 0.5
g = 9.81


#Tables materiaux
materialsList = [
	{"name":"Acier", "mv":7850, "e":210, "V": 0.33},
	{"name":"Aluminium", "mv":2700, "e":65, "V": 0.33},
	{"name":"Argent", "mv":10500, "e":78, "V": 0.37},
	{"name":"Béton", "mv":2400, "e":30, "V": 0.20},
	{"name":"Bois", "mv":800, "e":11, "V": 0.4},
	{"name":"Fonte", "mv":7100, "e":105, "V": 0.29},
	{"name":"Zinc", "mv":7150, "e":78, "V": 0.21},
	{"name":"Verre", "mv":4500, "e":60, "V": 0.25},
	{"name":"Caoutchouc", "mv":1800, "e":0.2, "V": 0.2},
	{"name":"Marbre", "mv":4000, "e":7, "V": 0.3},
	{"name":"Fonte", "mv":7100, "e":105, "V": 0.29}
]

'''
0 = angle
1 = lb
2 = b
3 = h
4 = lc
5 = lf
6 = material
7 = bf
8 = hf
'''

def generateIndividuals(n):
	panel = []
	for i in range(0,n):

		material = random.randint(0,len(materialsList)-1)

		individual = [
			random.randint(0,90),				#angle
			random.randint(0,50)/10,			#longueur du bras
			random.randint(0,50)/100,			#base
			random.randint(0,50)/100,			#hauteur
			random.randint(0,100)/10,			#longueur de corde
			random.randint(10,50)/10,			#longueur de la flèche
			material,
			random.randint(0,100)/1000,			#base de la flèche
			random.randint(0,100)/1000			#hauteur de la flèche
		]
		panel.append(individual)
	return panel

def calculateReach(individual):
	if(individual[4] > individual[2]):
		return 0
	k = (1/3)*(materialsList[individual[6]]["e"]/(1-2*materialsList[individual[6]]["V"])) #Ressort K (en N/m)
	try:
		lv = 0.5*sqrt(pow(individual[1],2)-pow(individual[4],2)) #Longueur à vide (en m)
		if(lv > individual[5]):
			return 0
	except ValueError:
		return 0
	ld = individual[5] - lv #Longueur du déplacement (en m)
	mp = materialsList[individual[6]]["mv"]*individual[7]*individual[8]*individual[5] #Masse du projectile (en kg)
	try:
		v = sqrt((k*pow(ld,2))/mp) #Vélocité V (en m/s)
	except ZeroDivisionError:
		return 0
	except ValueError:
		return 0
	I = (individual[2]*pow(individual[3],3))/12
	F = k*ld
	f = (F*pow(individual[1], 3))/48*materialsList[individual[6]]["e"]*I
	if ld > f:
		return 0
	reach = (pow(v,2)/g)*sin(radians(2*individual[0]%360)) #Portée P (en m)
	return reach


def calculateVelocity(individual):
	if(individual[4] > individual[2]):
		return 0
	k = (1/3)*(materialsList[individual[6]]["e"]/(1-2*materialsList[individual[6]]["V"])) #Ressort K (en N/m)
	try:
		lv = 0.5*sqrt(pow(individual[1],2)-pow(individual[4],2)) #Longueur à vide (en m)
		if(lv > individual[5]):
			return 0
	except ValueError:
		return 0
	ld = individual[5] - lv #Longueur du déplacement (en m)
	mp = materialsList[individual[6]]["mv"]*individual[7]*individual[8]*individual[5] #Masse du projectile (en kg)
	try:
		return sqrt((k*pow(ld,2))/mp) #Vélocité V (en m/s)
	except ZeroDivisionError:
		return 0
	except ValueError:
		return 0

def calculateKineticEnergy(individual):
	mp = materialsList[individual[6]]["mv"]*individual[7]*individual[8]*individual[5]
	ec = (1/2)*mp*pow(calculateVelocity(individual),2)
	return ec

def fitness(panel):
	fitnessedPanel = []
	for i in range(0,len(panel)):
		c.writerow({panel[0][0],panel[0][1],panel[0][2],panel[0][3],panel[0][4],panel[0][5],panel[0][7],panel[0][8]})
		weight = abs(calculateReach(panel[i]) - targetDistance)

		fitnessedPanel.append(weight)
	averageDistancePoints.append(average(fitnessedPanel))
	firstQuartilePoints.append(average(fitnessedPanel))
	thirdQuartilePoints.append(percentile(fitnessedPanel, 25))
	thirdQuartilePoints.append(percentile(fitnessedPanel, 75))
	kineticPoints.append(calculateKineticEnergy(panel[0])/1000)
	return fitnessedPanel


def createCouples(population):
	couples = []
	for n in range(0, int(populationSize/2)):
		contestants = random.sample(range(len(population)), 4)

		if population[contestants[0]] < population[contestants[1]]:
			firstParent = contestants[0]
		else:
			firstParent = contestants[1]

		if population[contestants[2]] < population[contestants[3]]:
			secondParent = contestants[2]
		else:
			secondParent = contestants[3]

		couples.append([firstParent, secondParent])
	return couples

def createChildrens(couples):
	newGeneration = []
	for i, couple in enumerate(couples):
		firstChild = []
		firstChild.append(panel[couple[0]][0])
		firstChild.append(panel[couple[0]][1])
		firstChild.append(panel[couple[0]][2])
		firstChild.append(panel[couple[0]][3])
		firstChild.append(panel[couple[0]][4])
		firstChild.append(panel[couple[1]][5])
		firstChild.append(panel[couple[1]][6])
		firstChild.append(panel[couple[1]][7])
		firstChild.append(panel[couple[1]][8])

		if random.randint(0,100) >= 100 - mutationChances:
			mutatedGene = random.randint(0,len(firstChild)-1)
			if mutatedGene == 0:
				firstChild[mutatedGene] = random.randint(0,90)
			elif mutatedGene == 1:
				firstChild[mutatedGene] = random.randint(0,50)/10
			elif mutatedGene == 2:
				firstChild[mutatedGene] = random.randint(0,50)/100
			elif mutatedGene == 3:
				firstChild[mutatedGene] = random.randint(0,50)/100
			elif mutatedGene == 4:
				firstChild[mutatedGene] = random.randint(0,100)/10
			elif mutatedGene == 5:
				firstChild[mutatedGene] = random.randint(10,50)/10
			elif mutatedGene == 6:
				firstChild[mutatedGene] = random.randint(0,len(materialsList)-1)
			elif mutatedGene == 7:
				firstChild[mutatedGene] = random.randint(0,100)/1000
			elif mutatedGene == 8:
				firstChild[mutatedGene] = random.randint(0,100)/1000

		secondChild = []
		secondChild.append(panel[couple[0]][0])
		secondChild.append(panel[couple[1]][1])
		secondChild.append(panel[couple[0]][2])
		secondChild.append(panel[couple[1]][3])
		secondChild.append(panel[couple[0]][4])
		secondChild.append(panel[couple[1]][5])
		secondChild.append(panel[couple[0]][6])
		secondChild.append(panel[couple[1]][7])
		secondChild.append(panel[couple[0]][8])

		if random.randint(0,100) >= 100 - mutationChances:
			mutatedGene = random.randint(0,len(secondChild)-1)
			if mutatedGene == 0:
				secondChild[mutatedGene] = random.randint(0,90)
			elif mutatedGene == 1:
				secondChild[mutatedGene] = random.randint(0,50)/10
			elif mutatedGene == 2:
				secondChild[mutatedGene] = random.randint(0,50)/100
			elif mutatedGene == 3:
				secondChild[mutatedGene] = random.randint(0,50)/100
			elif mutatedGene == 4:
				secondChild[mutatedGene] = random.randint(0,100)/10
			elif mutatedGene == 5:
				secondChild[mutatedGene] = random.randint(10,50)/10
			elif mutatedGene == 6:
				secondChild[mutatedGene] = random.randint(0,len(materialsList)-1)
			elif mutatedGene == 7:
				secondChild[mutatedGene] = random.randint(0,100)/1000
			elif mutatedGene == 8:
				secondChild[mutatedGene] = random.randint(0,100)/1000

		newGeneration.append(firstChild)
		newGeneration.append(secondChild)

	return newGeneration

panel = generateIndividuals(populationSize)

for i in range(numberOfGenerations):
	print("GENERATION " + str(i))
	fitnessedPanel = fitness(panel)
	couples = createCouples(fitnessedPanel)
	panel = createChildrens(couples)

	for truc in range(10):
			print(calculateReach(panel[truc]))



'''
0 = angle
1 = lb
2 = b
3 = h
4 = lc
5 = lf
6 = material
7 = bf
8 = hf

'''
print("=========================================")
print("ANGLE: " + str(panel[0][0]) + "°")
print("VELOCITÉ: " + str(calculateVelocity(panel[0])))
print("LONGUEUR DU BRAS: " + str(panel[0][1]) + " mètres")
print("LONGUEUR DE CORDE: " + str(panel[0][4]) + " mètres")
print("LONGUEUR DE LA FLECHE: " + str(panel[0][5]) + " mètres")
print("BASE DE LA FLECHE: " + str(panel[0][7]*100) + " centimètres")
print("HAUTEUR DE LA FLECHE: " + str(panel[0][8]*100) + " centimètres")
print("FLECHE EN " + str(materialsList[panel[0][6]]["name"]))
print(str(calculateKineticEnergy(panel[0])/1000) + " KG DE TNT")
print("=========================================")


plt.plot(averageDistancePoints)
#plt.plot(kineticPoints)
plt.ylabel('Distance from target (in meters)')
plt.xlabel('Generation')
plt.show()